<?php

/*
 * Предназначен для чтения и отображения превью входящего файла
 */

namespace App\Classes;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Illuminate\Support\Facades\Storage;

class XlsReader {
    public function __construct($file) {
        // Получить путь к файлу относительно корневого каталога
        $filePath = Storage::disk('local')->path($file);
        
        // Выполнить чтение файла, используя автовыбор reader'a для соответствующего формата
        $reader = IOFactory::createReaderForFile($filePath);
        $xlsxDoc = $reader->load($filePath);

        // Получить активную страницу
        $activeSheet = $xlsxDoc->getActiveSheet();

        dd($activeSheet);
    }
}
