<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\XlsReader;

class XlsPreviewBuilderController extends Controller
{
    public function show()
    {
        new XlsReader(config('xlsreader.file'));
        return view('xlspreview');
    }
}
